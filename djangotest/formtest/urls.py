from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^mfa/$', views.mfa, name='mfa'),
    url(r'^home/$', views.home, name='home'),
    # url(r'^display/$', views.display, name='display'),
    # url(r'^search/$', views.search, name='search'),
]
