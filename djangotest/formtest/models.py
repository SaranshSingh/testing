from django.db import models

# Create your models here.
class ImageDocument(models.Model):
    image_file = models.FileField(upload_to='documents/%Y/%m/%d')
