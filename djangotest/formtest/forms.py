from django import forms
from django.contrib.auth.models import User

# class ContactForm(forms.Form):
#     """Create a new form to enter details"""
#
#     subject = forms.CharField()
#     email = forms.CharField(required=False)
#     message = forms.CharField()
#
class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')

class MFAForm(forms.Form):
    passcode = forms.CharField()

class ImageForm(forms.Form):
    image_file = forms.FileField(label='Select a File')
