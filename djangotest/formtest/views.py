from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse

from .models import ImageDocument
from .forms import ImageForm, LoginForm, MFAForm

import requests
import json

# Create your views here.

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            headers = {
                    'Authorization': 'SSWS 00uCho5hundG8mWYxuVRTDtEs0yzeei_7n4_0Np122',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            payload = {"username": username, "password": password}
            data = json.dumps(payload)

            url = 'https://ifp.okta.com/api/v1/authn'

            response = requests.post(url, data=data, headers=headers)

            request.session['stateToken'] = response.json()['stateToken']
            request.session['state'] = response.json()['status']
            request.session['factors'] = response.json()['_embedded']['factors']
            request.session['url'] = response.json()['_embedded']['factors'][0]['_links']['verify']['href']

            # if response.status_code == 200:
            #     return render(request, 'formtest/response.html', {'response': response.text})

            return redirect('/mfa/')
            # if response.status_code == 200:
            #     return render(request, 'formtest/thanks.html')
            # else:
            #     return render(request, 'formtest/error.html')

    else:
        form = LoginForm()

    return render(request, 'formtest/login.html', {'form': form})

def mfa(request):
    state_token = request.session['stateToken']

    if request.method == 'POST':
        form = MFAForm(request.POST)
        if form.is_valid():
            passcode = int(form.cleaned_data['passcode'])

            payload = {"stateToken": state_token, "passCode": passcode}
            data = json.dumps(payload)

            # url = "https://ifp.okta.com/api/v1/authn/factors/uftft6a8dwLqmkbA22p6/verify"
            url = request.session['url']

            headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }

            response = requests.post(url, data=data, headers=headers)
            res = response.json()
            
            request.session['sessionToken'] = res['sessionToken']
            request.session['user_id'] = res['_embedded']['user']['id']
            print(request.session['user_id'])
            #add conditions to flush session information if status code not correct

            #gives the cookies for this url
            # lets_try_url = "https://ifp.okta.com/login/sessionCookieRedirect?token=" + res['sessionToken'] + "&redirectUrl=https%3A%2F%2Fifp.okta.com/"

            # return redirect(lets_try_url)
            # lets_try = requests.get(lets_try_url, headers=headers)
            #
            # print(lets_try.status_code)
            #gives the cookies for this url
            # return render(request, 'formtest/response.html', {'response': lets_try.cookies})

            return redirect('/home/')
    else:
        form = MFAForm()

    return render(request, 'formtest/mfa.html', {'form': form})



def home(request):
    session_token = request.session['sessionToken']

    headers = {
            'Authorization': 'SSWS 00uCho5hundG8mWYxuVRTDtEs0yzeei_7n4_0Np122',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    payload = json.dumps({"sessionToken": session_token})

    # url = "https://ifp.okta.com/api/v1/sessions?additionalFields=cookieToken"
    url = "https://ifp.okta.com/api/v1/sessions"

    response = requests.post(url, data=payload, headers=headers)

    return render(request, 'formtest/response.html', {'response': response.cookies})


    session_id = response.json()['id']

#Validate Session
    url_1 = "https://ifp.okta.com/api/v1/sessions/" + session_id

    session = requests.get(url_1, headers=headers)

#Display users' apps
    user_id = session.json()['userId']

    # user_id = request.session['user_id']
    #
    url_apps = "https://ifp.okta.com/api/v1/users/" + user_id + "/appLinks"


    applications = requests.get(url_apps, headers=headers)
    apps = applications.json()

    return render(request, 'formtest/home.html', {'apps': apps})


# def homer(request):
#     user_id = "00uf0ebjh3dENNf2P2p6"
#
#     headers = {
#                 'Authorization': 'SSWS 00uCho5hundG8mWYxuVRTDtEs0yzeei_7n4_0Np122',
#                 'Accept': 'application/json',
#                 'Content-Type': 'application/json'
#             }
#
#     url_apps = "https://ifp.okta.com/api/v1/users/" + user_id + "/appLinks"
#
#     applications = requests.get(url_apps, headers=headers)
#     apps = applications.json()
#
#     return render(request, 'formtest/home.html', {'apps': apps})
#
#
# def display(request):
#
#     if request.method == 'POST':
#         form = ImageForm(request.POST, request.FILES)
#         if form.is_valid:
#             new_image = ImageDocument(image_file=request.FILES['image_file'])
#             new_image.save()
#
#             return HttpResponse('<h1>Hello World</h1>')
#
#     else:
#         form = ImageForm()
#     document = ImageDocument.objects.all()
#
#     return render(request, 'formtest/list.html', {'form': form,
#                                                 'document': document})
#
#
# # def home(request):
# #     # return render(request, 'formtest/home.html')
# #     values = request.META
# #     html = []
# #     for k in sorted(values):
# #         html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, values[k]))
# #
# #     return HttpResponse('<table>%s</table>' % '\n'.join(html))
# #
# # def search_form(request):
# #     return render(request, 'formtest/search_form.html')
# #
# # def search(request):
# #     error = False
# #     if 'search' in request.GET:
# #         message = 'You searched for ""{text}""'.format(text=request.GET['search'])
# #     else:
# #         message = "You submitted an empty form"
# #
# #     return HttpResponse(message)
#
# # def search(request):
# #     error = False
# #     if 'search' in request.GET:
# #         search = request.GET['search'])
# #         if not search:
# #             error = True
# #     else:
# #         result = Book.ojects.filter(title_icontains=search)
# #         return render(request, 'formset/search_results.html', {'result': result})
# #
# #     return render(request, 'formset/search_form.html', {'errors': error})
